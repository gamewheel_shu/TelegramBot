
const Telegraf = require('telegraf');
const Extra = require('telegraf');
const Markup = require('telegraf');

const BotToken  = "319364692:AAGbw-NKCelIAphYX-RovXe_OU39rPSCtSI";

const gameShortName = 'Testgame';
const gameUrl = 'https://demo.gamewheel.com/tapTiles/toyCar2/';

const bot = new Telegraf(BotToken);

bot.command('start', function(ctx){ ctx.replyWithGame(gameShortName) });

bot.command('foo', function(ctx){
    return ctx.replyWithGame(gameShortName, Extra.markup(
        Markup.inlineKeyboard([
            Markup.gameButton('🎮 Play now!'),
            Markup.urlButton('Telegraf help', 'http://telegraf.js.org')
        ])))
});

bot.gameQuery( function(ctx) {
    console.log('Game query:', ctx.callbackQuery.game_short_name);
    return ctx.answerCallbackQuery(null, gameUrl)
});

bot.startPolling(60);